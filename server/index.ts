import { InMemoryCrudAdapter} from "./domain/crud";
import {Issuers as ModelIssuers} from "./domain/issuer/issuer.model";

export const Issuers =  new ModelIssuers(new InMemoryCrudAdapter())
