import {assertMethod, CompatibilityEvent, sendRedirect, useBody, useCookie} from "h3";
import {JwksClient} from "jwks-rsa"
import jwt, {JwtPayload, VerifyErrors, VerifyOptions} from "jsonwebtoken"
import {Issuers} from "../";
import assert from "assert";

export default async (req: CompatibilityEvent, res: CompatibilityEvent) => {
    assertMethod(req, 'POST', true);
    const body = await useBody(req)

    if (!body) {
        return sendRedirect(res, "/")
    }
    console.log("body", body)
    const decoded = jwt.decode(body.id_token, {complete: true})
    console.log("DECODED", decoded)
    const payload = decoded.payload;
    if(typeof payload === 'string') {
        throw new Error(`Wrong payload type (string) : ${payload}`)
    }
    const options: VerifyOptions = {
        audience: payload.aud,
        issuer: payload.iss,
    };
    const issuer = await Issuers.read(payload.iss)
    return new Promise(async (resolve, reject) => {
        if (decoded.header.alg === "HS256") {
            options.algorithms = ["HS256"]
            jwt.verify(body.id_token, issuer.clientSecret, options, async function (err: VerifyErrors | null, verifiedToken: JwtPayload) {
                assert(!err, err)
                assert(useCookie(req, "nonce") === verifiedToken.nonce, "nonce mismatch")
                resolve(await sendRedirect(res, "/"))
            });
        } else if (decoded.header.alg === "RS256" && decoded.header.kid) {
            options.algorithms = ["RS256"]
            const client = new JwksClient({
                jwksUri: issuer.jwksUri
            });

            const signingKey = (await client.getSigningKey(decoded.header.kid)).getPublicKey()
            jwt.verify(body.id_token, signingKey, options, async function (err: VerifyErrors | null, verifiedToken: JwtPayload) {
                assert(!err, err)
                assert(useCookie(req, "nonce") === verifiedToken.nonce, "nonce mismatch")
                resolve(await sendRedirect(res, "/"))
            });
        } else {
            reject("Unsupported algorithm")
        }
    })
}
