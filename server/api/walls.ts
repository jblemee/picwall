import {Walls} from "../domain/wall/wall.model";
import {InMemoryCrudAdapter} from "../domain/crud";

export default async (req, res) => {
    return new Walls(new InMemoryCrudAdapter()).all()
}
