import {sendRedirect, setCookie, useQuery} from "h3";
import {BaseClient, generators, Issuer as OIDIssuer} from "openid-client";
import {Issuer, IssuerId, JwksUri} from "../domain/issuer/issuer.model";
import {Issuers} from "~/server";

export default async (req, res) => {
    const {issuer} = useQuery(req)
    console.log(issuer)
    const oidcIssuer = await OIDIssuer.webfinger(issuer).catch(err=> {
        return OIDIssuer.discover(issuer)
    }).catch(_=> OIDIssuer.discover('https://accounts.google.com/'))
    console.log(oidcIssuer)
    const existingIssuer = await Issuers.read(oidcIssuer.issuer as IssuerId)
    const createIssuer = async () : Promise<BaseClient> => {
        const client = await (oidcIssuer.Client as BaseClient).register({
            client_name:"PicWall",
            response_types: ['id_token', 'token', 'code'],
            redirect_uris: ['http://picwall.lemee.co:3000/api/callback'],
            id_token_signed_response_alg: 'RS256',
            id_token_encrypted_response_alg: 'RS256',
        })
        console.log(client)
        await Issuers.create({
            id:oidcIssuer.issuer as IssuerId,
            clientId: client.client_id,
            clientSecret: client.client_secret,
            jwksUri: oidcIssuer.jwks_uri as JwksUri,
        })

        return client;
    }
    const client = existingIssuer ?
        new oidcIssuer.Client({
            client_id: existingIssuer.clientId,
            client_secret: existingIssuer.clientSecret,
            redirect_uris: ['http://picwall.lemee.co:3000/api/callback'],
            response_types: ['id_token', 'token', 'code'],
            id_token_signed_response_alg: 'RS256',
            id_token_encrypted_response_alg: 'RS256',
        }) : await createIssuer()

    console.trace("Client", client)

    const nonce = generators.nonce();
    const authorizeUrl = client.authorizationUrl({
        scope: 'openid',
        nonce,
        response_type: 'id_token',
        response_mode: 'form_post',
    });
    setCookie(res, 'nonce', nonce, {
        maxAge: 60,
        httpOnly: true,
        path: '/',
    });
    return sendRedirect(res, authorizeUrl  )
}
