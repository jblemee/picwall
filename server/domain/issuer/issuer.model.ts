import {CrudModel} from "..";
import {Crud} from "../crud";

export type IssuerId = string;
export type ClientID = string;
export type ClientSecret = string;
export type JwksUri = string;

export class Issuer {
    constructor(public id: IssuerId, public clientId: ClientID, public clientSecret: ClientSecret, public jwksUri : JwksUri)  {}
}
export interface IssuerAdapter extends Crud<IssuerId, Issuer> { }

export class Issuers extends CrudModel<IssuerId, Issuer, IssuerAdapter>{}
