import assert from "assert";
export interface Model<ID> {id: ID}

export interface Crud<ID, M extends Model<ID>> {
  create(data: M): Promise<M>;
  read(id: ID): Promise<M>;
  update(id: ID, data: M): Promise<M>;
  delete(id: ID): Promise<void>;
  all(): Promise<M[]>
}

export class InMemoryCrudAdapter<ID, M extends Model<ID>> implements Crud<ID, M> {
  private data: M[] = [];
  all(): Promise<M[]> {
    return Promise.resolve(this.data);
  }

  create(obj: M): Promise<M> {
    this.data.push(obj)
    return Promise.resolve(obj)
  }

  delete(id: ID): Promise<void> {
    this.data = this.data.filter(o => o.id !== id)
    return Promise.resolve()
  }

  read(id: ID): Promise<M|undefined> {
    return Promise.resolve(this.data.find(o => o.id === id))
  }

  update(id: ID, obj: M): Promise<M> {
    assert(obj.id === id)
    this.data =  this.data.map(w => w.id === id ? obj : w)
    return Promise.resolve(obj);
  }
}
