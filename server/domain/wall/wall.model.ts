import {CrudModel} from "..";
import {randomUUID} from "crypto";
import {Crud} from "../crud";

export type WallId = string;
export type WallName = string;
export class Wall {
    id: WallId;
    name: WallName

    constructor(name: WallName) {
        this.id = randomUUID()
        this.name = name;
    }
}
export interface WallAdapter extends Crud<WallId, Wall> { }

export class Walls extends CrudModel<WallId, Wall, WallAdapter>{}
