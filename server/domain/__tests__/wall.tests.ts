import {Wall, Walls} from "../wall/wall.model";
import {describe, expect, it} from "@jest/globals";
import {InMemoryCrudAdapter} from "../crud";

describe("Wall", () => {
  it("should create a wall", async () => {
    const wall = new Wall("test");
    const wall2 = new Wall("test 2");
    const walls = new Walls(new InMemoryCrudAdapter());
    await walls.create(wall);
    await walls.create(wall2);

    await expect(walls.read(wall.id)).resolves.toEqual(wall);
    await expect(walls.read(wall2.id)).resolves.toEqual(wall2);
    await expect(walls.read("random")).resolves.toBeUndefined();
    await expect(walls.all()).resolves.toEqual([wall, wall2]);
  });
});
