import {Crud, Model} from "./crud";

export class CrudModel<ID, M extends Model<ID>, Adapter extends Crud<ID, M> > implements Crud<ID, M>{
  constructor(private adapter: Adapter) {}
  all(): Promise<M[]> {
    return this.adapter.all();
  }

  read(id: ID): Promise<M> {
    return this.adapter.read(id);
  }

  create(model: M): Promise<M> {
    return this.adapter.create(model);
  }

  delete(id: ID): Promise<void> {
    return this.adapter.delete(id);
  }

  update(id: ID, model: M): Promise<M> {
    return this.adapter.update(id, model);
  }
}

